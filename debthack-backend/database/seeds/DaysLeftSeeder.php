<?php

use Illuminate\Database\Seeder;

class DaysLeftSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('days_left')->insert([
        	'days' => 7
        ]);
    }
}
