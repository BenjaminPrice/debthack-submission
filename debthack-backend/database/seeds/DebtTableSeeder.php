<?php

use Illuminate\Database\Seeder;

class DebtTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::first()->debts()->create([
            'accountName' => 'RBC CREDIT LINE',
            'accountHolder' => 'TRISTAN GEMUS',
            'runningBalance' => '17429.41',
            'type' => 3
        ]);
    }
}
