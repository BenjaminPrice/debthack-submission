<?php

use Illuminate\Database\Seeder;
use App\Account;
use App\Transaction;
use App\Keyword;
use App\Yodlee\Yodlee;

class TransactionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$Yodlee = new Yodlee();
    	//$transactions = $Yodlee->get_account_transactions();

        $transactions = json_decode(File::get(storage_path() . "/transactions.json"));

        $account = Account::create([
            'accountName' => 'TD GREEN VISA',
            'accountHolder' => 'TRISTAN GEMUS',
            'runningBalance' => '256.33',
            'type' => 1
        ]);

    	foreach ($transactions as $transaction) {
    		$account->transactions()->create([
    			'transactionId'=> isset( $transaction->viewKey->transactionId ) ? $transaction->viewKey->transactionId : null,
				'localizedTransactionBaseType'=> isset( $transaction->localizedTransactionBaseType ) ? $transaction->localizedTransactionBaseType : null,
				'cardAccountId'=> isset( $transaction->account->itemAccountId ) ? $transaction->account->itemAccountId : null,
				'postdate'=> isset( $transaction->postDate ) ? $transaction->postDate : null,
				'description'=> isset( $transaction->description->description ) ? $transaction->description->description : null,
				'categorizationKeyword'=> isset( $transaction->categorizationKeyword ) ? $transaction->categorizationKeyword : null,
				'amount'=> isset( $transaction->amount->amount ) ? $transaction->amount->amount : null,
				'keyword_id' => $this->get_keyword_id(isset($transaction->categorizationKeyword) ? $transaction->categorizationKeyword : null)
    		]);
    	}
    }

    public function get_keyword_id($keyword) {
        if (null !== $keyword) {
            if ($result = Keyword::where('name', $keyword)->first()) {
                return $result->id;
            } else {
                return 1;
            }
        } else {
            return 1;
        }
    }
}
