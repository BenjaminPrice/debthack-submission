<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
        	'user_name' => 'Tristan'
        ]);

        $user->accounts()->attach(1);
        $user->debts()->create([
            'name' => 'RBC CREDIT LINE',
            'interestRate' => 12.99,
            'balance' => 17429.41,
            'minPayment' => 45.33,
            'type' => 3
        ]);
    }
}
