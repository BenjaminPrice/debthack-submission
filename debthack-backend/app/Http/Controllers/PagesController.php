<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Yodlee\Yodlee;
use App\Account;
use App\Transaction;
use App\Keyword;
use File;

class PagesController extends Controller
{
    public function index()
    {
    	return Yodlee::getCobrandSessionToken();
    }

    public function get_keyword_id($keyword) {
    	if (null !== $keyword) {
    		if ($result = Keyword::where('name', $keyword)->first()) {
    			return $result->id;
    		} else {
    			return 1;
    		}
    	} else {
    		return 1;
    	}
    }

    public function test()
    {
    	$Yodlee = new Yodlee();

        $t = $Yodlee->get_account_transactions();

        $json = json_encode($t->searchResult->transactions);

        $fp = fopen(storage_path() . '/transactions.json', 'w');
        fwrite($fp, $json);
        fclose($fp);

        return 'Done!';
    }

    public function open()
    {
        $json = File::get(storage_path() . "/transactions.json");

        return print_r(json_decode($json), true);
    }

    public function export_json() {
        $dates = ['02-06-2016'];
    }

    public function api(Request $request)
    {
    	$Yodlee = new Yodlee();
    	$transactions = $Yodlee->get_account_transactions();

        $account = Account::create([
            'accountName' => 'TD GREEN VISA',
            'accountHolder' => 'TRISTAN GEMUS',
            'runningBalance' => '256.33'
        ]);

    	foreach ($transactions as $transaction) {
    		// return print_r($transaction, true);
    		$account->transactions()->create([
    			'transactionId'=> isset( $transaction->viewKey->transactionId ) ? $transaction->viewKey->transactionId : null,
				'localizedTransactionBaseType'=> isset( $transaction->localizedTransactionBaseType ) ? $transaction->localizedTransactionBaseType : null,
				'cardAccountId'=> isset( $transaction->account->itemAccountId ) ? $transaction->account->itemAccountId : null,
				'postdate'=> isset( $transaction->postDate ) ? $transaction->postDate : null,
				'description'=> isset( $transaction->description->description ) ? $transaction->description->description : null,
				'categorizationKeyword'=> isset( $transaction->categorizationKeyword ) ? $transaction->categorizationKeyword : null,
				'amount'=> isset( $transaction->amount->amount ) ? $transaction->amount->amount : null,
				'keyword_id' => $this->get_keyword_id(isset($transaction->categorizationKeyword) ? $transaction->categorizationKeyword : null)
    		]);

    	}

    	return print_r($account->transactions, true);
    }

    public function transactions()
    {
    	$Yodlee = new Yodlee();
    	// $accounts = $Yodlee->get_item_summaries_new();

    	$transactions = $Yodlee->get_account_transactions();

    	$html = '';

    	foreach ($transactions as $transaction) {
    		if (isset($transaction->categorizationKeyword)) {
    			$html .= $transaction->categorizationKeyword . '<br />';
    		}
    	}

    	return $html;

    	return '<pre>'.print_r($transactions, true).'</pre>';
    }
}
