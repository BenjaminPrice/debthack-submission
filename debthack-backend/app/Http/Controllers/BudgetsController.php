<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Alternative;
use App\Transaction;
use DB;

class BudgetsController extends Controller
{
    protected $dividers = array(
    	1 => 52 / 52, //weekly
    	2 => 52 / 26, // bi-weekly
    	3 => 52 / 104, // semi-weekly
    	4 => 52 / 12, // monthly
    	5 => 52 / 6, // bi-monthly
    	6 => 52 / 4, // quarterly
    	7 => 52  // annually
    );

    public function create_or_update(Request $request)
    {
    	if (null === $request->input('user_id')) {
    		return response()->json([
    			'response' => 'success',
    			'created' => false,
    			'message' => 'You must supply a user id'
    		]);
    	}

    	$user = User::find($request->input('user_id'));

    	if (!$user) {
    		return response()->json([
    			'response' => 'success',
    			'created' => false,
    			'message' => 'User does not exist!'
    		]);
    	}

    	$weekly_income = $this->calculate_weekly_income($user->income) - $this->calculate_weekly_bills($user->bills) - $this->calculate_weekly_min_payments($user->debts);

    	if (null !== $user->budget) {
    		$user->budget()->update([
	    		'weekly_income' => $request->input('weekly_income'),
	    		'weekly_budget' => $request->input('weekly_budget'),
	    		'towards_debt' => $request->input('towards_debt'),
	    		'towards_savings' => $request->input('towards_savings'),
	    		'budget_percent' => $request->input('budget_percent'),
	    		'debt_percent' => $request->input('debt_percent')
	    	]);

	    	$budget = $user->budget;

	    	return response()->json([
	    		'response' => 'success',
	    		'type' => 'updated',
	    		'budget' => $budget
	    	]);
    	} else {
    		$budget = $user->budget()->create([
	    		'weekly_income' => $weekly_income,
	    		'weekly_budget' => $weekly_income * .25,
	    		'towards_debt' => $weekly_income * .375,
	    		'towards_savings' => $weekly_income * .375,
	    		'budget_percent' => 25,
	    		'debt_percent' => 50
	    	]);

	    	return response()->json([
	    		'response' => 'success',
	    		'type' => 'created',
	    		'budget' => $budget
	    	]);
    	}
    }

    public function retrieve($user_id)
    {
    	$user = User::find($user_id);

    	if (!$user) {
    		return response()->json([
    			'response' => 'success',
    			'budget' => false,
    			'message' => 'User does not exist!'
    		]);
    	}

    	if (null === $user->budget) {
    		return response()->json([
    			'response' => 'success',
    			'budget' => false,
    			'message' => 'A budget does not exist for this user yet'
    		]);
    	}

    	$budget = $user->budget;
    	$budget->alteration_savings = $this->calculate_alternatives_total($user_id);

    	return response()->json([
    		'response' => 'success',
    		'budget' => $budget
    	]);
    }

    public function calculate_weekly_income($income_sources)
    {
    	$weekly_income = 0;

    	foreach ($income_sources as $income) {
    		$weekly_income += $income->amount / $this->dividers[$income->frequency];
    	}

    	return $weekly_income;
    }

    public function calculate_weekly_bills($bills)
    {
    	$weekly_bills = 0;

    	foreach ($bills as $bill) {
    		$weekly_bills += $bill->amount / $this->dividers[$bill->frequency];
    	}

    	return $weekly_bills;
    }

    public function calculate_weekly_min_payments($debts)
    {
    	$min_payments = 0;

    	foreach ($debts as $debt) {
    		$min_payments += $debt->minPayment / $this->dividers[4];
    	}

    	return $min_payments;
    }

    public function get_user_alternatives()
    {
    	$user_id = 1;
    	$user = User::find($user_id);

    	for ($i = 0; $i < count($user->alternatives); $i++) {
			$user->alternatives[$i]->details = Alternative::find($user->alternatives[$i]->id)->flags()->first();
		}

		return response()->json([
			'response' => 'success',
			'alternatives' => $user->alternatives
		]);
    }

    public function calculate_alternatives_total($user_id)
    {
    	$total = 0;
    	$user = User::find($user_id);

    	for ($i = 0; $i < count($user->alternatives); $i++) {
			$user->alternatives[$i]->details = Alternative::find($user->alternatives[$i]->id)->flags()->first();
		}

    	foreach ($user->alternatives as $alternative) {
    		$keyword_total = $this->get_keyword_sum($alternative->details->keywords->first()->id);
    		$total += $keyword_total * ($alternative->details->pivot->savings_percent / 100);
    	}

    	return $total;
    }

    public function get_keyword_sum($keyword_id)
    {
    	return Transaction::where('keyword_id', $keyword_id)
    		->where('postdate', '>', date('Y-m-d', strtotime('-6 weeks')))
    		->lists('amount')->sum();
    }

    public function remaining_percent($user_id)
    {
    	$user = User::find($user_id);

    	$budget = $user->budget;

    	$transactions_sum = $user->live_transactions->lists('amount')->sum();

    	return response()->json([
    		'response' => 'success',
    		'percent_used' => ($transactions_sum / $budget->weekly_budget) * 100,
    		'weekly_budget' => $budget->weekly_budget
    	]);
    }

    public function days_left()
    {
    	return response()->json([
    		'response' => 'success',
    		'days' => DB::table('days_left')->pluck('days')[0]
    	]);
    }

    public function payoff_date($user_id)
    {
    	$user = User::find($user_id);

    	// get all debts
    	$debts = $user->debts;

    	// get average interest rate and total debt value
    	$rate = 0;
    	$principal = 0;

    	foreach ($debts as $debt) {
    		$rate += $debt->interestRate;
    		$principal += $debt->balance;
    	}

    	$rate = $rate / count($debts);
    	$rate = $rate / 100;
    	$rate = $rate / 12;

    	// get monthly payment amount
    	$payment = $user->budget->towards_debt;
    	$payment += $this->calculate_weekly_min_payments($debts);
    	$payment = $payment * (52/12);

    	// add alternatives total to monthly payment
    	$payment += $this->calculate_alternatives_total($user_id);

    	$num_monthly_payments = $this->calculate_monthly_payments($principal, $payment, $rate);

    	$timestamp = strtotime('+ ' . ceil($num_monthly_payments) . ' months');

    	return response()->json([
    		'month' => date('M', $timestamp),
    		'year' => date('Y', $timestamp)
    	]);
    }

    // interest rate must be divided by 12 before calling
    public function calculate_monthly_payments($principal, $payment, $rate)
    {
    	$left = log(pow((1-($principal * $rate) / $payment), -1));
		$right = log(1 + $rate);

		return $left / $right;
    }
}
