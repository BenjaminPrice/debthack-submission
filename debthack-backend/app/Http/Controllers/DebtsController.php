<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Debt;
use App\User;
use App\DebtType;

class DebtsController extends Controller
{
    public function store(Request $request)
    {
    	if ($message = $this->account_validation_failed($request->all())) {
    		return response()->json([
    			'response' => 'success',
    			'valid' => false,
    			'message' => $message
    		]);
    	} else {
    		$user = User::find($request->input('user_id'));

    		$account = $user->debts()->create([
    			'name' => $request->input('name'),
    			'balance' => $request->input('balance'),
    			'interestRate' => $request->input('interestRate'),
    			'minPayment' => $request->input('minPayment'),
    			'type' => $request->input('type'),
    		]);

    		return response()->json([
    			'response' => 'success',
    			'valid' => true,
    			'account' => $account
    		]);
    	}
    }

    public function get_user_debts($user_id)
    {
    	if ($user = User::find($user_id)) {
    		$debts = $user->debts;

    		for ($i = 0; $i < count($debts); $i++) {
    			$debts[$i]->type = DebtType::find($debts[$i]->type);
    		}

			return response()->json([
				'response' => 'success',
				'found' => true,
				'accounts' => $user->debts
			]);
		} else {
			return response()->json([
				'response' => 'success',
				'found' => false
			]);
		}
    }

    public function account_validation_failed($data)
    {
    	return false;
    }
}
