<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Debt;

class UsersController extends Controller
{
    public function store(Request $request)
    {
    	if (null !== $request->input('user_name')) {
    		if ($user = User::where('user_name', $request->input('user_name'))->first()) {
    			// user exists
    			return response()->json([
		    		'response' => 'success',
		    		'user' => $user,
		    		'created' => false,
		    		'message' => 'User already exists'
		    	]);
    		}

    		$user = User::create([
    			'user_name' => $request->input('user_name')
	    	]);

    		// assign two default accounts
    		// td green visa
    		// rbc line of credit
	    	$this->assign_default_accounts($user);
            $this->create_default_debt_account($user);

	    	return response()->json([
	    		'response' => 'success',
	    		'created' => true,
	    		'user' => $user
	    	]);
    	} else {
    		return response()->json([
	    		'response' => 'success',
	    		'created' => false,
	    		'Failed to crete user.'
	    	]);
    	}
    }

    public function retrieve($user_id)
    {
    	if ($user = User::find($user_id)) {
    		return response()->json([
    			'response' => 'success',
    			'found' => true,
    			'user' => $user
    		]);
    	} else {
    		return response()->json([
    			'response' => 'success',
    			'found' => false,
    			'message' => 'User not found.'
    		]);
    	}
    }

    public function create_default_debt_account($user)
    {
        $user->debts()->create([
            'name' => 'RBC CREDIT LINE',
            'balance' => 17429.41,
            'interestRate' => 4.99,
            'minPayment' => 56.54,
            'type' => 3
        ]);
    }

    public function assign_default_accounts($user)
    {
    	$user->accounts()->attach(1);
    }
}
