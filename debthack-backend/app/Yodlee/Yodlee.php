<?php

namespace App\Yodlee;

use App\Yodlee\YodleeRestClient;

class Yodlee {

	const BASE_URL = "https://rest.developer.yodlee.com/services/srest/restserver/v1.0";
	// Url for get the value cobSessionToken for the user.
	const URL_COBRAND_SESSION_TOKEN = "/authenticate/coblogin";
	// Url for get the value userSessionToken for the user.
	const URL_USER_SESSION_TOKEN = "/authenticate/login";
	// Url for get search Site
	const URL_SEARCH_SITES = "/jsonsdk/SiteTraversal/searchSite";
	// Url for get Site Login Form
	const URL_GET_SITE_LOGIN_FORM = "/jsonsdk/SiteAccountManagement/getSiteLoginForm";
	// Url for get itemSummaries
	const URL_GET_ITEM_SUMMARIES = "/jsonsdk/DataService/getItemSummaries";
    // Url Site Account Management: addSiteAccount 
	const URL_ADD_SITE_ACCOUNT = "/jsonsdk/SiteAccountManagement/addSiteAccount";
	// Get all item summaries
	const URL_GET_ITEM_SUMMARIES_NEW = '/jsonsdk/DataService/getItemSummariesWithoutItemData';
	// get item summary for specific item
	const URL_GET_ITEM_SUMMARIES_ONE = '/jsonsdk/DataService/getItemSummaryForItem1';
	// get account transactions
	const URL_GET_TRANSACTIONS = '/jsonsdk/TransactionSearchService/executeUserSearchRequest;';

	private $session_id;
	private $user_session_id;

	public function __construct()
	{
		$this->get_session_vars(
			env('YODLEE_COBRAND_LOGIN', ''),
			env('YODLEE_COBRAND_PASSWORD', ''),
			env('YODLEE_USERNAME', ''),
			env('YODLEE_PASSWORD', '')
		);
	}

	public function get_session_vars($cobrandLogin, $cobrandPassword, $username, $password)
	{
		$session = $this->getCobrandSessionToken($cobrandLogin, $cobrandPassword);
    	$this->session_id = $session['Body']->cobrandConversationCredentials->sessionToken;

    	$user = $this->getUserSessionToken(
    		$username,
    		$password,
    		$this->session_id
    	);

    	$this->user_session_id = $user['Body']->userContext->conversationCredentials->sessionToken;
	}

	public function getCobrandSessionToken($cobrandLogin, $cobrandPassword)
	{
		$url_cobrand_login = self::BASE_URL . self::URL_COBRAND_SESSION_TOKEN;
		$cobrand_login = array(
			'cobrandLogin' => $cobrandLogin,
			'cobrandPassword' => $cobrandPassword
		);

		$response_to_request = YodleeRestClient::post($url_cobrand_login, $cobrand_login);

		$response = array(
			"isValid"      => true,
			"Body"         => $response_to_request["Body"]
		);

		return $response;
	}

	public function getUserSessionToken($username, $password, $session_token)
	{
		$url_cobrand_login = self::BASE_URL . self::URL_USER_SESSION_TOKEN;
		$cobrand_login = array(
			'login' => $username,
			'password' => $password,
			'cobSessionToken' => $session_token
		);

		$response_to_request = YodleeRestClient::post($url_cobrand_login, $cobrand_login);

		$error_local = array_key_exists("Error", $response_to_request);

		if($error_local){
			$response = array(
				"isValid"      =>false,
				"ErrorServer"  => $response_to_request["Error"]
			);
		} else {
			$response = array(
				"isValid"      => true,
				"Body"         => $response_to_request["Body"]
			);
		}

		return $response;
	}

	public function get_item_summaries()
	{
		$url = self::BASE_URL . self::URL_GET_ITEM_SUMMARIES;
		$parameters = array(
			'cobSessionToken' => $this->session_id,
			'userSessionToken' => $this->user_session_id
		);

		$response_to_request = YodleeRestClient::post($url, $parameters);

		return $response_to_request;
	}

	public function get_item_summaries_new()
	{
		$url = self::BASE_URL . self::URL_GET_ITEM_SUMMARIES_NEW;
		$parameters = array(
			'cobSessionToken' => $this->session_id,
			'userSessionToken' => $this->user_session_id
		);

		return YodleeRestClient::post($url, $parameters);
	}

	public function get_specific_item_details($item_id)
	{
		$url = self::BASE_URL . self::URL_GET_ITEM_SUMMARIES_ONE;
		$parameters = array(
			'cobSessionToken' => $this->session_id,
			'userSessionToken' => $this->user_session_id,
			'itemId' => $item_id,
			'dex.startLevel' => 0,
			'dex.endLevel' => 0,
			'dex.extentLevels' => 4
		);

		return YodleeRestClient::post($url, $parameters);
	}

	public function get_account_transactions()
	{
		$url = self::BASE_URL . self::URL_GET_TRANSACTIONS;
		$parameters = array(
			'cobSessionToken' => $this->session_id,
			'userSessionToken' => $this->user_session_id,
			'transactionSearchRequest.containerType' => 'All',
			'transactionSearchRequest.higherFetchLimit' => 500,
			'transactionSearchRequest.lowerFetchLimit' => 1,
			'transactionSearchRequest.resultRange.startNumber' => 1,
			'transactionSearchRequest.resultRange.endNumber' => 200,
			'transactionSearchRequest.searchFilter.currencyCode' => 'CAD',
			'transactionSearchRequest.searchClients.clientId' => 1,
			'transactionSearchRequest.searchClients.clientName' => 'DataSearchService',
			'transactionSearchRequest.searchClients' => 'DEFAULT_SERVICE_CLIENT',
			'transactionSearchRequest.userInput' => '',
			'transactionSearchRequest.ignoreUserInput' => 'true',
			'transactionSearchRequest.ignoreManualTransactions' => 'false',
			'transactionSearchRequest.searchFilter.transactionSplitType' => 'ALL_TRANSACTION',
			'transactionSearchRequest.searchFilter.itemAccountId.identifier' => 10193292,
			'transactionSearchRequest.searchFilter.postDateRange.fromDate' => '01-01-2015',
			'transactionSearchRequest.searchFilter.postDateRange.toDate' => '02-06-2016',
		);

		$transactions = YodleeRestClient::post($url, $parameters);

		return $transactions['Body'];
	}

}