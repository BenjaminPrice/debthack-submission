<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flag extends Model
{
    protected $fillable = [
    	'title'
    ];

    public function alternatives()
    {
    	return $this->belongsToMany('App\Alternative')->withPivot('savings_percent', 'message');
    }

    public function keywords()
    {
    	return $this->belongsToMany('App\Keyword');
    }
}
