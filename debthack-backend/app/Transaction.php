<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
      'transactionId',
      'localizedTransactionBaseType',
      'cardAccountId',
      'postdate',
      'description',
      'categorizationKeyword',
      'amount',
    	'keyword_id'
   	];

   	public function account()
   	{
   		return $this->belongsTo('App\Account');
   	}

   	public function keyword()
   	{
   		return $this->belongsTo('App\Keyword', 'keyword_id');
   	}
}