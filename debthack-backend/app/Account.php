<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = [
    	'user_id',
    	'accountName',
    	'accountHolder',
        'type'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function transactions()
    {
    	return $this->hasMany('App\Transaction');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}
