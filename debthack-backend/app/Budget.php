<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Budget extends Model
{
    protected $fillable = [
    	'weekly_budget',
    	'weekly_income',
    	'towards_debt',
    	'towards_savings',
    	'budget_percent',
    	'debt_percent'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
