<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Debt extends Model
{
    protected $fillable = [
    	'balance',
    	'interestRate',
    	'minPayment',
    	'type',
        'name'
    ];

    public function user()
    {
    	return $this->belongsto('App\User');
    }
}
