<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_name',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    public function meta()
    {
        return $this->hasMany('App\Meta');
    }

    public function accounts()
    {
        return $this->belongsToMany('App\Account');
    }

    public function debts()
    {
        return $this->hasMany('App\Debt');
    }

    public function bills()
    {
        return $this->hasMany('App\Bill');
    }

    public function income()
    {
        return $this->hasMany('App\Income');
    }

    public function alternatives()
    {
        return $this->belongsToMany('App\Alternative')->withPivot('ignore');
    }

    public function budget()
    {
        return $this->hasOne('App\Budget');
    }

    public function live_transactions()
    {
        return $this->hasMany('App\LiveTransaction');
    }

    public function notifications()
    {
        return $this->hasMany('App\Notification');
    }


}
