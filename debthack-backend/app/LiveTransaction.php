<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveTransaction extends Model
{
    protected $fillable = [
    	'amount',
    	'keyword_id'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
