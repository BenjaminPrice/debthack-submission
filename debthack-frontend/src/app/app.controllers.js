var appControllers = angular.module('DebtHackControllers', ['angular-underscore', 'DebtHackBudgetCtrl', 'DebtHackLoginCtrl','DebtHackAlternativeCtrl','DebtHackFactory', 'DebtHackIncomeCtrl', 'DebtHackBillCtrl', 'DebtHackHomeCtrl', 'DebtHackCtrl', 'DebtHackDebtCtrl']);
var _firstLogin = false;

var homeCtrl = angular.module('DebtHackHomeCtrl', []);
homeCtrl.controller('homeCtrl', function($scope, homeFactory, alternativeFactory, userFactory, budgetFactory) {
    $scope.newAlternatives = 0;
    
    var getNewAlternatives = function () {
        if (userFactory.isLoggedIn()) {
            alternativeFactory.getNewAlternatives().then(function successCallback(response) {
                $scope.newAlternatives = response.data.flags.length;
            }, function errorCallback(response) {
                console.log(response);
            });
        }
        else
            $scope.goToLogin();
    }
    getNewAlternatives();
    
    var getNotifications = function () {
        if (userFactory.isLoggedIn()) {
            homeFactory.getNotifications().then(function successCallback(response) {
                console.log(response);
                $scope.notifications = response.data.notifications;
            }, function errorCallback(response) {
                console.log(response);
            });
        }
        else
            $scope.goToLogin();
    }
    getNotifications();
    
    $scope.dismissNotification = function(notification_id) {
        homeFactory.dismissNotification(notification_id).then(function successCallback(response) {
                getNotifications();
            }, function errorCallback(response) {
                console.log(response);
            }
        );
    }
    
    $scope.budgetRemaining = {
        percent_used: 0,
        weekly_budget: 0
    };
    
    var getBudgetRemaining = function () {
        if (userFactory.isLoggedIn()) {
            budgetFactory.getBudgetRemaining().then(function successCallback(response) {
                $scope.budgetRemaining = {
                    percent_used: response.data.percent_used,
                    weekly_budget: response.data.weekly_budget
                };
            }, function errorCallback(response) {
                console.log(response);
            });
        }
        else
            $scope.goToLogin();
    }
    getBudgetRemaining();
    
    var getBudgetDays = function () {
        if (userFactory.isLoggedIn()) {
            budgetFactory.getBudgetDays().then(function successCallback(response) {
                $scope.budgetDaysRemaining = response.data.days;
            }, function errorCallback(response) {
                console.log(response);
            });
        }
        else
            $scope.goToLogin();
    }
    getBudgetDays();
});

var budgetCtrl = angular.module('DebtHackBudgetCtrl', []);
budgetCtrl.controller('budgetCtrl', function($scope, budgetFactory, userFactory) {
    var getBudget = function () {
            if(userFactory.isLoggedIn())
            {
                budgetFactory.getBudget().then(function successCallback(response) {
                        $scope.budget = response.data.budget;
                    }, function errorCallback(response) {
                        console.log(response);
                    }
                );
            }
            else
                $scope.goToLogin();
        }
    getBudget();
        
    $scope.updateBudget = function() {
            var budget = {
                budget_percent: $scope.budget.budget_percent,
                debt_percent: $scope.budget.debt_percent,
                weekly_income: $scope.budget.weekly_income,
                weekly_budget: $scope.budget.weekly_income * ($scope.budget.budget_percent / 100),
                towards_debt: ($scope.budget.weekly_income - ($scope.budget.weekly_income * ($scope.budget.budget_percent / 100))) * ((100 - $scope.budget.debt_percent) / 100),
                towards_savings: ($scope.budget.weekly_income - ($scope.budget.weekly_income * ($scope.budget.budget_percent / 100))) * ($scope.budget.debt_percent / 100)
            };
            budgetFactory.updateBudget(budget);
            if(_firstLogin)
                _firstLogin = !_firstLogin;
            $scope.goToHome();
        };
});

var loginCtrl = angular.module('DebtHackLoginCtrl', []);
loginCtrl.controller('loginCtrl', function($scope, userFactory, sharedFactory) {
    $scope.doLogin = function() {
        userFactory.doLogin($scope.userName).then(function successCallback(response) {
                userFactory.setUserToken(response.data.user);
                userFactory.setCreatedFlag(response.data.created);
                $scope.isLoggedIn = true;
                _firstLogin = userFactory.isFirstLogin();
                if(_firstLogin)
                    $scope.goToDebts();
                else
                    $scope.goToHome();
            }, function errorCallback(response) {
                userFactory.setUserToken(null);
                console.log(response);
                $scope.goToLogin();
            }
        );

    }
});

var debtCtrl = angular.module('DebtHackDebtCtrl', []);
debtCtrl.controller('debtCtrl', function($scope, debtFactory, sharedFactory, userFactory) {
        var getDebts = function () {
            if(userFactory.isLoggedIn())
            {
                debtFactory.getDebts().then(function successCallback(response) {
                        $scope.debts = response.data.accounts;
                    }, function errorCallback(response) {
                        console.log(response);
                    }
                );
            }
            else
                $scope.goToLogin();
        }
        getDebts();

        sharedFactory.getDebtTypes().then(function successCallback(response) {
                        $scope.debtTypes = response.data.types;
                    }, function errorCallback(response) {
                        console.log(response);
                    }
                );
        
        $scope.pushDebt = function() {
            debtFactory.pushDebt($scope.newDebt).then(function successCallback(response) {
                    $scope.newDebt.name = null;
                    $scope.newDebt.balance = null;
                    $scope.newDebt.interestRate = null;
                    $scope.newDebt.minPayment = null;
                    $scope.newDebt.selectedDebtType = null;
                    getDebts();
                }, function errorCallback(response) {
                    console.log(response);
                }
            );
        }
        
    });
    
var incomeCtrl = angular.module('DebtHackIncomeCtrl', []);
incomeCtrl.controller('incomeCtrl', function($scope, incomeFactory, sharedFactory, userFactory, budgetFactory) {
        var getIncomeSources = function () {
            if(userFactory.isLoggedIn())
            {
                incomeFactory.getIncomeSources().then(function successCallback(response) {
                        $scope.incomeSources = response.data.accounts;
                    }, function errorCallback(response) {
                        console.log(response);
                    }
                );
            }
            else
                $scope.goToLogin();
        }
        getIncomeSources();
        
        sharedFactory.getFrequencyTypes().then(function successCallback(response) {
                    $scope.frequencies = response.data.types;
                }, function errorCallback(response) {
                    console.log(response);
                }
            );
                
        $scope.setupInitialBudget = function() {
            var budget = {
               
            };
            budgetFactory.updateBudget(budget);
            $scope.goToBudget();
        };
        
        $scope.pushIncomeSource = function() {
            incomeFactory.pushIncomeSource($scope.newIncomeSource).then(function successCallback(response) {
                    $scope.newIncomeSource.name = null;
                    $scope.newIncomeSource.amount = null;
                    $scope.newIncomeSource.selectedFrequency = null;
                    getIncomeSources();
                }, function errorCallback(response) {
                    console.log(response);
                }
            );
        }
        
    });
    
var billCtrl = angular.module('DebtHackBillCtrl', []);
billCtrl.controller('billCtrl', function($scope, billFactory, sharedFactory, userFactory) {
        var getBills = function () {
            if(userFactory.isLoggedIn())
            {
                billFactory.getBills().then(function successCallback(response) {
                        $scope.bills = response.data.accounts;
                    }, function errorCallback(response) {
                        console.log(response);
                    }
                );
            }
            else
                $scope.goToLogin();
        }
        getBills();   
        
        sharedFactory.getFrequencyTypes().then(function successCallback(response) {
                        $scope.frequencies = response.data.types;
                    }, function errorCallback(response) {
                        console.log(response);
                    }
                );
        
        $scope.pushBill = function() {
            billFactory.pushBill($scope.newBill).then(function successCallback(response) {
                    $scope.newBill.name = null;
                    $scope.newBill.amount = null;
                    $scope.newBill.selectedFrequency = null;
                    getBills();
                }, function errorCallback(response) {
                    console.log(response);
                }
            );
            
        }
        
    });
    
var alternativeCtrl = angular.module('DebtHackAlternativeCtrl', []);
alternativeCtrl.controller('alternativeCtrl', function($scope, alternativeFactory, sharedFactory, userFactory) {
    var getAlternatives = function () {
        if(userFactory.isLoggedIn())
        {
            alternativeFactory.getAlternatives().then(function successCallback(response) {
                    $scope.alternatives = response.data;
                }, function errorCallback(response) {
                    console.log(response);
                }
            );
        }
        else
            $scope.goToLogin();
    }
    getAlternatives();
        
    $scope.toggleContent = function(id) {
        var alternative = _.find($scope.alternatives, function(obj) { return obj.id == id; });
        alternative.showContent = !alternative.showContent;
    };
    
    $scope.selectAlternative = function(alternativeId, altId) {
        var alternative = _.find($scope.alternatives, function(obj) { return obj.id == alternativeId; });
        alternative.selectedAltId = altId;
        
        alternativeFactory.pushAlternative(alternative).then(function successCallback(response) {
                $scope.updatePayOffDate();
                getAlternatives();
            }, function errorCallback(response) {
                console.log(response);
            }
        );
    };
    
    
    $scope.pushIgnoreAlternative = function(alternative) {
        alternativeFactory.pushIgnoreAlternative(alternative).then(function successCallback(response) {
                getAlternatives();
            }, function errorCallback(response) {
                console.log(response);
            }
        );
    };
        
    });

var DebtHackCtrl = angular.module('DebtHackCtrl', []);
DebtHackCtrl.controller('DebtHackCtrl', function ($scope, $http, $window, $location, $mdSidenav, userFactory, sharedFactory) {
    $window.prerenderReady = false;
    $scope.renderReady = false;
    $scope.isLoggedIn = false;
    
    $scope.screenWidth = $window.innerWidth;

    $scope.tooltip = {};
    $scope.toggleRight = function() {
        $scope.tooltip.menu = false;
        $mdSidenav('right').toggle();
    }
    
    $scope.goToHome = function() {
        $location.path('/');
    };
    $scope.goToIncome = function() {
        $location.path('/income');
    };
    $scope.goToBills = function() {
        $location.path('/bills');
    };
    $scope.goToDebts = function() {
        $location.path('/debts');
    };
    
    $scope.goToLogin = function() {
        $location.path('/login');
    };
    
    $scope.goToAlternatives = function() {
        $location.path('/alternatives');
    };
    
    $scope.goToBudget = function() {
        $location.path('/budget');
    };
    
    $scope.doLogout = function() {
        userFactory.doLogout();
        $scope.isLoggedIn = false;
        $scope.goToLogin();
    }
    
    $scope.updatePayOffDate = function() {
        if(!_firstLogin)
            sharedFactory.getPayOffDate().then(function successCallback(response) {
                    $scope.payOffDate = response.data;
                }, function errorCallback(response) {
                    console.log(response);
                }
            );
    }
    
    $scope.checkLoggedIn = function() {
        $scope.firstLogin = _firstLogin;
        if(userFactory.isLoggedIn())
        {
            $scope.isLoggedIn = true;
            $scope.updatePayOffDate();
        }
        else {
            $scope.isLoggedIn = false;
            $scope.goToLogin();
        }
    }
});