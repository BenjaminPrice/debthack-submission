var appRoutes = angular.module('DebtHackRoutes', ['ngRoute']);

appRoutes.config(function ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/home.html',
        controller: 'homeCtrl'
      })
      .when('/debts', {
        templateUrl: 'views/debts.html',
        controller: 'debtCtrl'
      })
      .when('/bills', {
        templateUrl: 'views/bills.html',
        controller: 'billCtrl'
      })
      .when('/income', {
        templateUrl: 'views/income.html',
        controller: 'incomeCtrl'
      })
      .when('/alternatives', {
        templateUrl: 'views/alternatives.html',
        controller: 'alternativeCtrl'
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'loginCtrl'
      })
      .when('/budget', {
        templateUrl: 'views/budget.html',
        controller: 'budgetCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });

    $locationProvider.html5Mode(false);
  });

