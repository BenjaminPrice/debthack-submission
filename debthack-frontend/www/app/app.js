var app = angular.module('DebtHackApp', ['ngMaterial', 'angular-underscore', 'DebtHackRoutes', 'DebtHackControllers', 'DebtHackFactory', 'ngRoute']);

app.config(function($mdThemingProvider) {
    $mdThemingProvider.theme('default')
        .primaryPalette('green')
        .accentPalette('blue');
});

app.filter('percentage', ['$filter', function ($filter) {
  return function (input, decimals) {
    return $filter('number')(input, decimals) + '%';
  };
}]);