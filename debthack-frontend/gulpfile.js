var gulp = require('gulp'),
    jade = require('gulp-jade'),
    nodemon = require('gulp-nodemon');

var env = 'development';
var outputDir = 'www';

gulp.task('jade', function(){
    var config = {};
    if(env === 'development') {
        config.pretty = true;
    }
    return gulp.src('src/**/*.jade')
        .pipe(jade(config))
        .pipe(gulp.dest(outputDir));
});

gulp.task('js', function(){
    return gulp.src('src/**/*.js')
        .pipe(gulp.dest(outputDir));
});

gulp.task('bower', function(){
    return gulp.src('bower_components/**/*.*')
        .pipe(gulp.dest(outputDir + '/bower_components'));
});

gulp.task('img', function(){
    return gulp.src('/src/img/*.*')
        .pipe(gulp.dest(outputDir + '/img'));
});

gulp.task('css', function(){
    return gulp.src('src/**/*.css')
        .pipe(gulp.dest(outputDir));
});

gulp.task('watch', function(){
    gulp.watch('src/**/*.jade', ['jade']);
    gulp.watch('src/**/*.js', ['js']);
    gulp.watch('src/css/**/*.css', ['css']);
});

gulp.task('start', function () {
    nodemon({
        script: 'www/'
        , ext: 'js html css'
        , env: { 'NODE_ENV': 'development' }
    })
});

gulp.task('default', ['jade', 'js', 'css', 'sass', 'watch', 'start']);
